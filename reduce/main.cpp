#include <iostream>
#include <boost/program_options.hpp>
#include "cimgext/cimgext.h"
#include "util/cimg_series.h"

namespace po = boost::program_options;

int x, y, z, input_width, input_height, input_depth, output_width,
    output_height, output_depth;
std::string image_path, regex_filter, datatype, output_prefix, series_format;

template<class CIMG_TYPE>
int
process()
{
    cimg_library::CImg<CIMG_TYPE> image;
    if(!cimg_load_series<CIMG_TYPE>(image, image_path.c_str(),
        regex_filter.c_str())) return 1;
    if(!input_width) input_width = image.width();
    if(!input_height) input_height = image.height();
    if(!input_depth) input_depth = image.depth();
    if(!output_width) output_width = input_width;
    if(!output_height) output_height = input_height;
    if(!output_depth) output_depth = input_depth;
    std::cout << "Loaded image size: " << image.width() << " x " <<
        image.height() << " x " << image.depth() << std::endl;
    image.crop(x, y, z, x+input_width-1, y+input_height-1, z+input_depth-1);
    std::cout << "Cropped image size: " << image.width() << " x " <<
        image.height() << " x " << image.depth() << std::endl;
    image.resize(output_width, output_height, output_depth, image.spectrum(),
        5);
    std::cout << "Resized image size: " << image.width() << " x " <<
        image.height() << " x " << image.depth() << std::endl;
    cimg_save_series(image, output_prefix.c_str(), series_format.c_str());
    return 0;
}

int
main(int ac, char* av[])
{
    try
    {
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("image_path,i", po::value<std::string>(&image_path),
                "input isosurface image path (supports cimg formats)")
            ("regex_filter,I", po::value<std::string>(&regex_filter),
                "regex filter to select slices (optional)")
            ("output_prefix,o", po::value<std::string>(&output_prefix))
            ("series_format,f",
                po::value<std::string>(&series_format)->default_value(""),
                "output slices to series with this format")
            ("x,x", po::value<int>(&x)->default_value(0))
            ("y,y", po::value<int>(&y)->default_value(0))
            ("z,z", po::value<int>(&z)->default_value(0))
            ("input_width,w", po::value<int>(&input_width)->default_value(0))
            ("input_height,e", po::value<int>(&input_height)->default_value(0))
            ("input_depth,d", po::value<int>(&input_depth)->default_value(0))
            ("output_width,W", po::value<int>(&output_width)->default_value(0))
            ("output_height,E",
                po::value<int>(&output_height)->default_value(0))            
            ("output_depth,D", po::value<int>(&output_depth)->default_value(0))
            ("datatype,t", po::value<std::string>(&datatype)->
                default_value("float"), "uchar, short, float, double");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help") || image_path == "" || output_prefix == "")
        {
            std::cout << "Resize/crop input image (possibly stack)"
                << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
        if(datatype == "uchar") return process<unsigned char>();
        else if(datatype == "short") return process<short>();
        else if(datatype == "float") return process<float>();
        else if(datatype == "double") return process<double>();
        else
        {
            std::cerr << "ERROR: Unsupported datatype!" << std::endl;
            return 1;
        }

    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}

