#ifndef __OFF2MESH_H__
#define __OFF2MESH_H__

#include <iostream>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>
#include <CGAL/Polyhedral_mesh_domain_3.h>
#include <CGAL/make_mesh_3.h>
#include <CGAL/refine_mesh_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>

#include "util/cgalmesh.h"

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Polyhedron_3<K> Polyhedron;
typedef CGAL::Polyhedral_mesh_domain_3<Polyhedron, K> Mesh_domain;
typedef CGAL::Mesh_triangulation_3<Mesh_domain>::type Tr;
typedef CGAL::Mesh_complex_3_in_triangulation_3<Tr> C3t3;
typedef CGAL::Mesh_criteria_3<Tr> Mesh_criteria;

template<class FLOAT_TYPE>
int
off2mesh(const char *off_filename, const char *mesh_filename,
    FLOAT_TYPE facet_angle,
    FLOAT_TYPE facet_size,
    FLOAT_TYPE facet_distance,
    FLOAT_TYPE cell_radius_edge_ratio,
    FLOAT_TYPE cell_size,
    FLOAT_TYPE sliver_bound,
    FLOAT_TYPE time_limit,
    bool perturb,
    bool lloyd,
    bool odt,
    bool exude)
{
    if(facet_angle < 0)
    {
        std::cerr << "ERROR: facet_angle < 0!" << std::endl;
        return 1;
    }
    if(facet_size < 0)
    {
        std::cerr << "ERROR: facet_size < 0!" << std::endl;
        return 1;
    }
    if(facet_distance < 0)
    {
        std::cerr << "ERROR: facet_distance < 0!" << std::endl;
        return 1;
    }
    if(cell_radius_edge_ratio < 0)
    {
        std::cerr << "ERROR: cell_radius_edge_ratio < 0!" << std::endl;
        return 1;
    }
    if(cell_size < 0)
    {
        std::cerr << "ERROR: cell_size < 0!" << std::endl;
        return 1;
    }
    if(facet_angle > 30)
    {
        std::cout << "WARNING: not guaranteed to terminate with "
            "facet_angle > 30!" << std::endl;
    }
    if(cell_radius_edge_ratio > 2)
    {
        std::cout << "WARNING: not guaranteed to terminate with "
            "cell_radius_edge_ratio > 2!" << std::endl;
    }

    std::cout << "Generating CGAL mesh... " << std::flush;
    Polyhedron polyhedron;
    std::ifstream input(off_filename);
    input >> polyhedron;
    Mesh_domain domain(polyhedron);
    Mesh_criteria criteria(CGAL::parameters::facet_angle = facet_angle,
        CGAL::parameters::facet_size = facet_size,
        CGAL::parameters::facet_distance = facet_distance,
        CGAL::parameters::cell_radius_edge_ratio = cell_radius_edge_ratio,
        CGAL::parameters::cell_size = cell_size);
    C3t3 c3t3 = CGAL::make_mesh_3<C3t3>(domain, criteria,
        CGAL::parameters::no_perturb(), CGAL::parameters::no_exude());
    std::cout << "done!" << std::endl;
    if(perturb)
    {
        std::cout << "Perturbing... " << std::flush;
        std::cout << mesh_return_code(CGAL::perturb_mesh_3(c3t3, domain,
            CGAL::parameters::sliver_bound = sliver_bound,
            CGAL::parameters::time_limit = time_limit)) << std::endl;
    }
    if(lloyd)
    {
        std::cout << "Lloyd optimizing... " << std::flush;
        std::cout << mesh_return_code(CGAL::lloyd_optimize_mesh_3(c3t3, domain,
            CGAL::parameters::time_limit = time_limit,
            CGAL::parameters::do_freeze = false)) << std::endl;
    }
    if(odt)
    {
        std::cout << "Odt-smoothing... " << std::flush;
        std::cout << mesh_return_code(CGAL::odt_optimize_mesh_3(c3t3, domain,
            CGAL::parameters::time_limit = time_limit)) << std::endl;
    }
    if(exude)
    {
        std::cout << "Exuding... " << std::flush;
        std::cout << mesh_return_code(CGAL::exude_mesh_3(c3t3,
            CGAL::parameters::sliver_bound = sliver_bound,
            CGAL::parameters::time_limit = time_limit)) << std::endl;
    }
    std::cout << "Saving... " << std::flush;
    std::ofstream medit_file(mesh_filename);
    c3t3.output_to_medit(medit_file);
    std::cout << "done!" << std::endl;
    return 0;
}

#endif

