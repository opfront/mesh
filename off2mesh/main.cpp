#include <iostream>
#include <boost/program_options.hpp>
#include "off2mesh.h"

namespace po = boost::program_options;

int
main(int ac, char* av[])
{
    try
    {
        bool perturb = false, lloyd = false, odt = false, exude = false;
        double isovalue, facet_angle, facet_size, facet_distance,
            cell_radius_edge_ratio, cell_size, sliver_bound, time_limit;
        std::string off_filename, mesh_filename, datatype;
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("off_filename,i", po::value<std::string>(&off_filename),
                "input surface mesh filename (off format)")
            ("mesh_filename,o", po::value<std::string>(&mesh_filename),
                "output volumetric mesh filename (mesh format)")
            ("facet_angle,a",
                po::value<double>(&facet_angle)->default_value(30),
                "surface facet angle lower bound (degrees)")
            ("facet_size,f",
                po::value<double>(&facet_size)->default_value(1),
                "facet size upper bound (radius)")
            ("facet_distance,d",
                po::value<double>(&facet_distance)->default_value(0.2),
                "facet approximation error upper bound")
            ("cell_radius_edge_ratio,c",
                po::value<double>(&cell_radius_edge_ratio)->default_value(2),
                "tetrahedron circumradius and edge length ratio upper bound")
            ("cell_size,s",
                po::value<double>(&cell_size)->default_value(1000),
                "tetrahedron circumradius upper bound")
            ("sliver_bound,b",
                po::value<double>(&sliver_bound)->default_value(0),
                "lower bound on dihedral angles (exude and perturb options)")
            ("time_limit,l",
                po::value<double>(&time_limit)->default_value(0),
                "time limit (s) of perturb, lloyd, odt, and exude operations")
            ("perturb", po::value<bool>(&perturb)->multitoken()->zero_tokens(),
                "optimize using CGAL's perturb function")
            ("lloyd", po::value<bool>(&lloyd)->multitoken()->zero_tokens(),
                "optimize using Lloyd's algorithm")
            ("odt", po::value<bool>(&odt)->multitoken()->zero_tokens(),
                "optimize using Odt-smoothing")
            ("exude", po::value<bool>(&exude)->multitoken()->zero_tokens(),
                "optimize using CGAL's exude function")
            ("datatype,t", po::value<std::string>(&datatype)->
                default_value("float"), "uchar, short, float, double");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help") || off_filename == "" || mesh_filename == "")
        {
            std::cout << "Volumetric mesh from surface mesh" << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
        off2mesh<float>(off_filename.c_str(), mesh_filename.c_str(),
            facet_angle, facet_size, facet_distance, cell_radius_edge_ratio,
            cell_size, sliver_bound, time_limit, perturb, lloyd, odt, exude);
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}

