add_executable(off2mesh main.cpp)

target_link_libraries(off2mesh ${Boost_LIBRARIES})
target_link_libraries(off2mesh ${CGAL_LIBRARY})
target_link_libraries(off2mesh ${CGAL_Core_LIBRARY})
target_link_libraries(off2mesh ${GMP_LIBRARIES})
target_link_libraries(off2mesh ${MPFR_LIBRARIES})

#if(NiftiCLib_FOUND)
#    target_link_libraries(off2mesh ${NiftiCLib_LIBRARIES})
#endif(NiftiCLib_FOUND)

install(TARGETS off2mesh DESTINATION bin)
