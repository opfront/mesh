echo Construct surface mesh in off-format.
bin\img2off.exe -i ../../data/cropped.vol -o ../../data/cropped.off

echo Construct volumetric mesh in mesh-format directly from the image.
bin\img2mesh.exe -i ../../data/cropped.vol -o ../../data/cropped_direct.mesh
bin\img2mesh.exe -i ../../data/cropped.vol -o ../../data/cropped_direct_no_triangles.mesh -T

echo Construct volumetric mesh in mesh format indirectly from the surface mesh.
bin\off2mesh.exe -i ../../data/cropped.off -o ../../data/cropped_indirect.mesh
