#include <Python.h>
#include <iostream>
#include "img2off/img2off.h"
#include "img2mesh/img2mesh.h"
#include "off2mesh/off2mesh.h"

static PyObject*
img2off_c(PyObject *self, PyObject *args)
{
    const char *image_path, *regex_filter, *mesh_filename;
    int minimum_initial_points;
    float isovalue, angle, radius, distance, error, initial_points_ratio;
    if(!PyArg_ParseTuple(args, "sssfffffif", &image_path, &regex_filter,
        &mesh_filename, &isovalue, &angle, &radius, &distance, &error,
        &minimum_initial_points, &initial_points_ratio))
        return NULL;
    img2off<float>(image_path, regex_filter, mesh_filename, isovalue, angle,
        radius, distance, error, minimum_initial_points, initial_points_ratio);
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject*
off2mesh_c(PyObject *self, PyObject *args)
{
    const char *off_filename, *mesh_filename;
    int perturb, lloyd, odt, exude;
    float facet_angle, facet_size, facet_distance,
        cell_radius_edge_ratio, cell_size, sliver_bound, time_limit;
    if(!PyArg_ParseTuple(args, "ssfffffffiiii", &off_filename, &mesh_filename,
        &facet_angle, &facet_size, &facet_distance,
        &cell_radius_edge_ratio, &cell_size, &sliver_bound, &time_limit,
        &perturb, &lloyd, &odt, &exude))
        return NULL;
    off2mesh<float>(off_filename, mesh_filename,
        facet_angle, facet_size, facet_distance, cell_radius_edge_ratio,
        cell_size, sliver_bound, time_limit, perturb, lloyd, odt, exude);
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject*
img2mesh_c(PyObject *self, PyObject *args)
{
    const char *image_path, *regex_filter, *cell_size_path,
        *cell_size_regex_filter, *mesh_filename;
    int perturb, lloyd, odt, exude, minimum_initial_points, skip_triangles;
    float isovalue, facet_angle, facet_size, facet_distance,
        cell_radius_edge_ratio, cell_size, sliver_bound, initial_points_ratio,
        time_limit;
    if(!PyArg_ParseTuple(args, "sssfffffffiffiiiii", &image_path, &regex_filter,
        &mesh_filename, &isovalue, &facet_angle, &facet_size, &facet_distance,
        &cell_radius_edge_ratio, &cell_size, &cell_size_path,
        &cell_size_regex_filter, &sliver_bound,
        &minimum_initial_points, &initial_points_ratio, &time_limit, &perturb,
        &lloyd, &odt, &exude, &skip_triangles))
        return NULL;
    img2mesh<float, float>(image_path, regex_filter, mesh_filename, isovalue,
        facet_angle, facet_size, facet_distance, cell_radius_edge_ratio,
        cell_size, cell_size_path, cell_size_regex_filter, sliver_bound,
        minimum_initial_points, initial_points_ratio,
        time_limit, perturb, lloyd, odt, exude, skip_triangles);
    Py_INCREF(Py_None);
    return Py_None;
}

static PyMethodDef mesherMethods[] =
{
    {"img2off_c",  img2off_c, METH_VARARGS,
     "Create a surface mesh file from an isosurface image file"},
    {"off2mesh_c",  off2mesh_c, METH_VARARGS,
     "Create a volumetric mesh file from a surface mesh file"},
    {"img2mesh_c",  img2mesh_c, METH_VARARGS,
     "Create a volumetric mesh file from an isosurface image file"},
    {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC
initmesher(void)
{
    (void) Py_InitModule("mesher", mesherMethods);
}
