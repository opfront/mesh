#ifndef __OFF_H__
#define __OFF_H__

#include <fstream>

template<class FLOAT_TYPE, class INDEX_TYPE>
int
off_load(const char *filename,
    INDEX_TYPE &vertex_count,
    FLOAT_TYPE *&vertices,
    INDEX_TYPE &face_count,
    INDEX_TYPE *&faces)
{
    int temp;
    std::ifstream f(filename);
    if(!f.is_open()) return 0;
    std::string off;
    f >> off;
    if(off != "OFF")
    {
        std::cerr << "ERROR: reading " << filename << ". Not an off file!"
            << std::endl;
        return 0;
    }
    f >> vertex_count >> face_count >> temp;
    if(vertex_count) vertices = new FLOAT_TYPE[vertex_count*3];
    if(face_count) faces = new INDEX_TYPE[face_count*3];
    for(INDEX_TYPE i = 0; i < vertex_count*3; i++)
    {
        f >> vertices[i];
    }
    for(INDEX_TYPE i = 0; i < face_count; i++)
    {
        int n;
        f >> n;
        if(n != 3)
        {
            std::cerr << "ERROR: reading " << filename
                << ". Only faces with 3 vertices supported!"
                << std::endl;
            return 0;
        }
        f >> faces[i*3] >> faces[i*3+1] >> faces[i*3+2];
    }
    return 1;
}

#endif

