#ifndef __CGALMESH_H__
#define __CGALMESH_H__

#include <CGAL/Mesh_optimization_return_code.h>

std::string
mesh_return_code(CGAL::Mesh_optimization_return_code r)
{
    switch(r)
    {
    case CGAL::BOUND_REACHED:
        return std::string("Bound reached!");
    case CGAL::TIME_LIMIT_REACHED:
        return std::string("Time limit reached!");
    case CGAL::CANT_IMPROVE_ANYMORE:
        return std::string("Cannot improve anymore!");
    case CGAL::MAX_ITERATION_NUMBER_REACHED:
        return std::string("Max iteration number reached!");
    case CGAL::CONVERGENCE_REACHED:
        return std::string("Convergence reached!");
    case CGAL::ALL_VERTICES_FROZEN:
        return std::string("All vertices frozen!");
    default:
        return std::string("Unknown result!");
    }
}

#endif

