#ifndef __MESHTRAITS_H__
#define __MESHTRAITS_H__

#include <CGAL/Kernel/global_functions_3.h>

static const __thread void *boundary_value;
static const __thread void *isosurface_image;

typedef field_type (*function_type)(const point_type&);

// Callback function used by CGAL to get the isosurface image values.
template<class CIMG_TYPE>
field_type
isovalue_function(const point_type &p)
{
    const cimg_library::CImg<CIMG_TYPE> *img = 
        static_cast<const cimg_library::CImg<CIMG_TYPE>*>(isosurface_image);
    // We will enforce sharp boundaries at image borders, this is needed to
    // get straight mesh edges along image borders, but should perhaps be
    // possible to disable. If this is not done, the boundary value will be
    // interpolated cubicly in a voxel-wide interval outside the image border.
    //if(p[0] < 0 || p[1] < 0 || p[2] < 0 ||
    //   p[0] > img->width()-1 || p[1] > img->height()-1 || p[2] > img->depth())
    //    return *static_cast<const CIMG_TYPE*>(boundary_value);

    return static_cast<const cimg_library::CImg<CIMG_TYPE>*>(
        isosurface_image)->cubic_atXYZ(p[0], p[1], p[2], 0,
            *static_cast<const CIMG_TYPE*>(boundary_value));
}

// Finds a reasonable bounding sphere to the iso-surface.
template<class CIMG_TYPE>
sphere_type
bounding_sphere(const cimg_library::CImg<CIMG_TYPE> &isovalue_image)
{
    point_type mn(isovalue_image.width(), isovalue_image.height(),
        isovalue_image.depth()), mx(0, 0, 0);
    cimg_forXYZ(isovalue_image, x, y, z)
    {
        if(isovalue_image(x, y, z) <= 0) continue;
        // CGAL only provides constant accessors.
        if(x < mn[0]) mn = point_type(x, mn[1], mn[2]);
        if(x > mx[0]) mx = point_type(x, mx[1], mx[2]);
        if(y < mn[1]) mn = point_type(mn[0], y, mn[2]);
        if(y > mx[1]) mx = point_type(mx[0], y, mx[2]);
        if(z < mn[2]) mn = point_type(mn[0], mn[1], z);
        if(z > mx[2]) mx = point_type(mx[0], mx[1], z);
    }
    point_type center;
    field_type squared_radius = std::numeric_limits<field_type>::max();
    cimg_forXYZ(isovalue_image, x, y, z)
    {
        if(isovalue_image(x, y, z) <= 0) continue;
        float mn0 = x-mn[0], mn1 = y-mn[1], mn2 = z-mn[2],
              mx0 = x-mx[0], mx1 = y-mx[1], mx2 = z-mx[2];
        float d0 = mn0*mn0+mn1*mn1+mn2*mn2,
              d1 = mx0*mx0+mx1*mx1+mx2*mx2;
        field_type d = std::max(d0, d1);
        if(d < squared_radius)
        {
            center = point_type(x, y, z);
            squared_radius = d;
        }
    }
    // Pad with at least a diagonal voxel.
    squared_radius = sqrt(squared_radius)+3;
    squared_radius *= squared_radius;
    // TBD! For some reason more closely bounded radii make the mesher crash.
    squared_radius = isovalue_image.width()*isovalue_image.width()+
        isovalue_image.height()*isovalue_image.height()+
        isovalue_image.depth()*isovalue_image.depth();

    std::cout << "Radius: " << sqrt(squared_radius) << " "
        << "center: " << center[0] << " " << center[1] << " " << center[2]
        << std::endl;
    return sphere_type(center, squared_radius);
}

template<class SURFACE_TYPE, class CIMG_TYPE>
class surface_mesh_traits_type : public SURFACE_TYPE::Surface_mesher_traits_3
{
    const cimg_library::CImg<CIMG_TYPE> *image;
public:
    typedef typename SURFACE_TYPE::Surface_mesher_traits_3 base;
    typedef typename base::Geom_traits geom_traits_type;
    typedef typename geom_traits_type::FT field_type;

    const int minimum_initial_points;
    const field_type initial_points_ratio;

    surface_mesh_traits_type(const cimg_library::CImg<CIMG_TYPE> *image_,
        int minimum_initial_points_, field_type initial_points_ratio_) :
        image(image_),
        minimum_initial_points(minimum_initial_points_),
        initial_points_ratio(initial_points_ratio_)
    {
    }

    class Construct_initial_points
    {
        typedef typename base::Point_3 point_type;
        typedef typename base::Surface_3 surface_type;

        typedef surface_mesh_traits_type<SURFACE_TYPE, CIMG_TYPE> self_type;
        const self_type& oracle;

    public:
        Construct_initial_points(const self_type& oracle_) : oracle(oracle_)
        {
        }

        template <typename OutputIteratorPoints>
        void
        add_point(OutputIteratorPoints &out, const surface_type &surface,
            double x0, double y0, double z0,
            double x1, double y1, double z1)
        {
            typename self_type::Intersect_3 intersect =
                oracle.intersect_3_object();
            point_type a(x0, y0, z0), b(x1, y1, z1);
            CGAL::Object o = intersect(surface,
                typename self_type::Segment_3(a, b));
            if(const point_type *intersection = CGAL::object_cast<point_type>(
                &o)) *out++ = *intersection;
        }

        // Random points
        template <typename OutputIteratorPoints>
        OutputIteratorPoints operator() (const surface_type& surface,
            OutputIteratorPoints out,
            int placeholder)
        {
            typename self_type::Intersect_3 intersect =
                oracle.intersect_3_object();
            cimg_library::CImg<int> labels(oracle.image->width(),
                oracle.image->height(), oracle.image->depth());
            cimg_forXYZ(labels, x, y, z)
            {
                labels(x, y, z) = intersect.surf_equation(surface,
                    point_type(x, y, z)) > 0;
            }
            labels.label();
            int labels_max = labels.max();
            std::vector<int> surface_points(labels_max, 0);
            // Compute surface areas of regions.
            cimg_forXYZ(labels, x, y, z)
            {
                int l = labels(x, y, z)-1;
                if(l == -1) continue;
                if(!labels.atXYZ(x-1, y, z)) surface_points[l]++;
                if(!labels.atXYZ(x+1, y, z)) surface_points[l]++;
                if(!labels.atXYZ(x, y-1, z)) surface_points[l]++;
                if(!labels.atXYZ(x, y+1, z)) surface_points[l]++;
                if(!labels.atXYZ(x, y, z-1)) surface_points[l]++;
                if(!labels.atXYZ(x, y, z+1)) surface_points[l]++;
            }
            std::vector<std::set<int> > selected_points(
                labels_max, std::set<int>());
            // Choose n distinct random surface points of each region.
            for(int i = 0; i < labels_max; i++)
            {
                std::srand(i);
                int m = std::min(static_cast<int>(
                    oracle.minimum_initial_points+
                    surface_points[i]*oracle.initial_points_ratio),
                    surface_points[i]);
                while(static_cast<int>(selected_points[i].size()) < m)
                {
                    selected_points[i].insert(static_cast<int>(
                        surface_points[i]*static_cast<double>(std::rand())/
                        RAND_MAX));
                }
            }
            // Store the iterators to avoid searching the sets.
            std::vector<std::set<int>::iterator> selected_points_i(
                labels_max);

            for(int i = 0; i < labels_max; i++)
            {
                // Add maximum to avoid having to check for end of set.
                selected_points[i].insert(std::numeric_limits<int>::max());

                selected_points_i[i] = selected_points[i].begin();
                surface_points[i] = 0;
            }
            // Now add the selected surface points.
            cimg_forXYZ(labels, x, y, z)
            {
                int l = labels(x, y, z)-1;
                if(l == -1) continue;
                if(!labels.atXYZ(x-1, y, z) &&
                   surface_points[l]++ == *selected_points_i[l])
                {
                    add_point(out, surface, x, y, z, x-1, y, z);
                    selected_points_i[l]++;
                }
                if(!labels.atXYZ(x+1, y, z) &&
                   surface_points[l]++ == *selected_points_i[l])
                {
                    add_point(out, surface, x, y, z, x+1, y, z);
                    selected_points_i[l]++;
                }
                if(!labels.atXYZ(x, y-1, z) &&
                   surface_points[l]++ == *selected_points_i[l])
                {
                    add_point(out, surface, x, y, z, x, y-1, z);
                    selected_points_i[l]++;
                }
                if(!labels.atXYZ(x, y+1, z) &&
                   surface_points[l]++ == *selected_points_i[l])
                {
                    add_point(out, surface, x, y, z, x, y+1, z);
                    selected_points_i[l]++;
                }
                if(!labels.atXYZ(x, y, z-1) &&
                   surface_points[l]++ == *selected_points_i[l])
                {
                    add_point(out, surface, x, y, z, x, y, z-1);
                    selected_points_i[l]++;
                }
                if(!labels.atXYZ(x, y, z+1) &&
                   surface_points[l]++ == *selected_points_i[l])
                {
                    add_point(out, surface, x, y, z, x, y, z+1);
                    selected_points_i[l]++;
                }
            }
            return out;
        }
    }; // end nested class Construct_initial_points

    Construct_initial_points construct_initial_points_object()
        const
    {
        return Construct_initial_points(*this);
    }
};

#endif

