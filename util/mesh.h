#ifndef __MESH_H__
#define __MESH_H__

#include <assert.h>
#include <fstream>

template<class FLOAT_TYPE, class INDEX_TYPE>
int
mesh_load(const char *filename,
    INDEX_TYPE &vertex_count,
    FLOAT_TYPE *&vertices,
    INDEX_TYPE &triangle_count,
    INDEX_TYPE *&triangles)
{
    int temp;
    std::ifstream f(filename);
    if(!f.is_open()) return 0;
    std::string mesh;
    f >> mesh;
    if(mesh != "MeshVersionFormatted")
    {
        std::cerr << "ERROR: reading " << filename << ". Not a mesh file!"
            << std::endl;
        return 0;
    }
    int mesh_version;
    f >> mesh_version;
    if(mesh_version != 1)
    {
        std::cerr << "ERROR: reading " << filename
            << ". Unknown mesh file version!" << std::endl;
        return 0;
    }
    std::string line;
    while(std::getline(f, line))
    {
        if(line == "Dimension")
        {
            int dimension;
            f >> dimension;
            if(dimension != 3)
            {
                std::cerr << "ERROR: reading " << filename
                    << ". Expected 3-dimensional mesh, found: "
                    << dimension << "-dimensional!" << std::endl;
                return 0;
            }
        }
        else if(line == "Vertices")
        {
            FLOAT_TYPE temp;
            f >> vertex_count;
            // For some reason vertex_count is off by one.
            vertex_count++;

            vertices = new FLOAT_TYPE[vertex_count*3];
            for(int i = 0; i < vertex_count; i++)
            {
                if(!std::getline(f, line))
                {
                    std::cerr << "ERROR: reading " << filename
                        << ". Premature end to vertex list!" << std::endl; 
                    return 0;
                }
                std::stringstream(line) >> vertices[i*3] >> vertices[i*3+1]
                    >> vertices[i*3+2] >> temp;
            }
        }
        else if(line == "Tetrahedra")
        {
            INDEX_TYPE temp;
            int tetrahedra_count;
            if(!std::getline(f, line))
            {
                std::cerr << "ERROR: reading " << filename
                    << ". Premature end to tetrahedra list!" << std::endl; 
                return 0;
            }
            std::stringstream(line) >> tetrahedra_count;
            triangle_count = tetrahedra_count*4;
            triangles = new INDEX_TYPE[triangle_count*3];
            int mx = 0;
            for(int i = 0; i < tetrahedra_count; i++)
            {
                if(!std::getline(f, line))
                {
                    std::cerr << "ERROR: reading " << filename
                        << ". Premature end to tetrahedra list!" << std::endl; 
                    return 0;
                }
                int t0, t1, t2, t3;
                std::stringstream(line) >> t0 >> t1 >> t2 >> t3 >> temp;
                assert(t0 >= 0 && t0 < vertex_count);
                assert(t1 >= 0 && t1 < vertex_count);
                assert(t2 >= 0 && t2 < vertex_count);
                assert(t3 >= 0 && t3 < vertex_count);
                triangles[i*4*3] = t0;
                triangles[i*4*3+1] = t1;
                triangles[i*4*3+2] = t2;

                triangles[i*4*3+3] = t0;
                triangles[i*4*3+4] = t1;
                triangles[i*4*3+5] = t3;

                triangles[i*4*3+6] = t0;
                triangles[i*4*3+7] = t2;
                triangles[i*4*3+8] = t3;

                triangles[i*4*3+9] = t1;
                triangles[i*4*3+10] = t2;
                triangles[i*4*3+11] = t3;
            }
        }
    }
    return 1;
}

#endif

