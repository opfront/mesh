#ifndef __VSTAT_H__
#define __VSTAT_H__

#include <iostream>
#include <sstream>

namespace vstat
{
    template<class TYPE>
    TYPE
    sum(const std::vector<TYPE>& values)
    {
        TYPE sum = 0;
        for(int i = 0; i < values.size(); i++) sum += values[i];
        return sum;
    }

    template<class TYPE>
    TYPE
    mean(const std::vector<TYPE>& values)
    {
        return sum(values)/values.size();
    }

    template<class TYPE>
    TYPE
    max(const std::vector<TYPE>& values)
    {
        TYPE m = -std::numeric_limits<TYPE>::max();
        for(int i = 0; i < values.size(); i++)
        {
            if(values[i] > m) m = values[i];
        }
        return m;
    }

    template<class TYPE>
    TYPE
    min(const std::vector<TYPE>& values)
    {
        TYPE m = std::numeric_limits<TYPE>::max();
        for(int i = 0; i < values.size(); i++)
        {
            if(values[i] < m) m = values[i];
        }
        return m;
    }

    template<class TYPE>
    TYPE
    stddev(const std::vector<TYPE>& values)
    {
        TYPE mn = mean(values), sdsum = 0;
        for(int i = 0; i < values.size(); i++)
        {
            sdsum += (values[i]-mn)*(values[i]-mn);
        }
        return sqrt(sdsum/values.size());
    }

    template<class TYPE>
    std::string
    summary(const std::vector<TYPE>& values)
    {
        std::stringstream ss;
        ss << "min: " << vstat::min(values)
           << " max: " << vstat::max(values)
           << " avg: " << vstat::mean(values)
           << " stddev: " << vstat::stddev(values);
        return ss.str();
    }
}

#endif

