#ifndef __MESHDOMAIN_H__
#define __MESHDOMAIN_H__

#include <CGAL/make_mesh_3.h>
#include <CGAL/Implicit_to_labeling_function_wrapper.h>
#include <CGAL/Labeled_mesh_domain_3.h>
#include <CGAL/Implicit_mesh_domain_3.h>
//#include <CGAL/Kernel/global_functions_3.h>

template<typename Function, class BGT, class CIMG_TYPE>
class mesh_domain : public CGAL::Labeled_mesh_domain_3<
    CGAL::Implicit_to_labeling_function_wrapper<Function,BGT>, BGT>
{
public:
    typedef CGAL::Implicit_to_labeling_function_wrapper<Function, BGT>
        wrapper;
    /// base type
    typedef typename CGAL::Labeled_mesh_domain_3<wrapper, BGT> base;

    /// Public types
    typedef typename base::Sphere_3 sphere_type;
    typedef typename base::FT field_type;
    
    const cimg_library::CImg<CIMG_TYPE> *image;
    int minimum_initial_points;
    field_type initial_points_ratio;
    Function function_;
    wrapper wrapper_;

    // Note the use of this construct function and the self initialization
    // (wrapper_(wrapper_) in the constructor below is to make sure that the
    // wrapper object is kept allocated, because the base object (CGAL code)
    // only stores reference to it and will therefore segfault otherwise.
    wrapper&
    construct_wrapper(Function f)
    {
        function_ = f;
        new (&wrapper_) wrapper(function_);
        return wrapper_;
    }

    /**
     * Constructor
     * @param f the function which negative values defines the domain
     * @param bounding_sphere a bounding sphere of the domain
     * @param error_bound the error bound relative to the sphere radius
     */
    mesh_domain(const Function f,
            const cimg_library::CImg<CIMG_TYPE> *image_,
            const sphere_type& bounding_sphere,
            const field_type& error_bound,
            int minimum_initial_points_,
            field_type initial_points_ratio_)
        : base(construct_wrapper(f), bounding_sphere,
          error_bound), image(image_),
          minimum_initial_points(minimum_initial_points_),
          initial_points_ratio(initial_points_ratio_), wrapper_(wrapper_)
    {

    }

    /// Destructor
    virtual ~mesh_domain() {}

    /**
     * Constructs  a set of \ccc{n} points on the surface, and output them to
     *  the output iterator \ccc{pts} whose value type is required to be
     *  \ccc{std::pair<Points_3, Index>}.
     */
    class Construct_initial_points
    {
        typedef typename base::Point_3 point_type;
        typedef typename base::Segment_3 segment_type;
        typedef mesh_domain<Function, BGT, CIMG_TYPE> self_type;
        const self_type& oracle;

    public:
        Construct_initial_points(const self_type& oracle_)
            : oracle(oracle_) {}

        template <typename OutputIteratorPoints>
        void
        add_point(OutputIteratorPoints &out,
            double x0, double y0, double z0,
            double x1, double y1, double z1) const
        {
            point_type a(x0, y0, z0), b(x1, y1, z1);
            segment_type segment(a, b);
            typename base::Surface_patch surface =
                oracle.do_intersect_surface_object()(segment);
            if(!surface) return;
            const point_type intersect_pt = CGAL::cpp11::get<0>(
                oracle.construct_intersection_object()(segment));
            *out++ = std::make_pair(intersect_pt,
                oracle.index_from_surface_patch_index(*surface));
        }

        template<class OutputIterator>
            OutputIterator operator()(OutputIterator pts, const int n = 12)
                const;
    };

    /// Returns Construct_initial_points object
    Construct_initial_points construct_initial_points_object() const
    {
        return Construct_initial_points(*this);
    }

private:
    // Disabled copy constructor & assignment operator
    mesh_domain(const mesh_domain& src);
    mesh_domain& operator=(const mesh_domain& src);
};  // end class mesh_domain



template<class F, class BGT, class CIMG_TYPE>
template<class OutputIterator>
OutputIterator
mesh_domain<F, BGT, CIMG_TYPE>::Construct_initial_points::operator()(
    OutputIterator out, const int nb_points) const
{
    cimg_library::CImg<int> labels(oracle.image->width(),
            oracle.image->height(), oracle.image->depth());
    cimg_forXYZ(labels, x, y, z)
    {
        labels(x, y, z) = (oracle.function_)(point_type(x, y, z)) > 0;
    }
    labels.label();
    // Swap background label with 0 label
    int background = 0;
    cimg_forXYZ(labels, x, y, z)
    {
        if((oracle.function_)(point_type(x, y, z)) > 0)
        {
            background = labels(x, y, z);
            break;
        }
    }
    cimg_forXYZ(labels, x, y, z)
    {
        if(labels(x, y, z) == background) labels(x, y, z) = 0;
        else if(labels(x, y, z) == 0) labels(x, y, z) = background;
    }
    int labels_max = labels.max();
    std::vector<int> surface_points(labels_max, 0);
    // Compute surface areas of regions.
    cimg_forXYZ(labels, x, y, z)
    {
        int l = labels(x, y, z)-1;
        if(l == -1) continue;
        if(!labels.atXYZ(x-1, y, z)) surface_points[l]++;
        if(!labels.atXYZ(x+1, y, z)) surface_points[l]++;
        if(!labels.atXYZ(x, y-1, z)) surface_points[l]++;
        if(!labels.atXYZ(x, y+1, z)) surface_points[l]++;
        if(!labels.atXYZ(x, y, z-1)) surface_points[l]++;
        if(!labels.atXYZ(x, y, z+1)) surface_points[l]++;
    }
    std::vector<std::set<int> > selected_points(
            labels_max, std::set<int>());
    // Choose n distinct random surface points of each region.
    for(int i = 0; i < labels_max; i++)
    {
        std::srand(i);
        int m = std::min(static_cast<int>(
                    oracle.minimum_initial_points+
                    surface_points[i]*oracle.initial_points_ratio),
                    surface_points[i]);
        while(static_cast<int>(selected_points[i].size()) < m)
        {
            selected_points[i].insert(static_cast<int>(
                        surface_points[i]*static_cast<double>(std::rand())/
                        RAND_MAX));
        }
    }
    // Store the iterators to avoid searching the sets.
    std::vector<std::set<int>::iterator> selected_points_i(
            labels_max);

    for(int i = 0; i < labels_max; i++)
    {
        // Add maximum to avoid having to check for end of set.
        selected_points[i].insert(std::numeric_limits<int>::max());

        selected_points_i[i] = selected_points[i].begin();
        surface_points[i] = 0;
    }
    // Now add the selected surface points.
    cimg_forXYZ(labels, x, y, z)
    {
        int l = labels(x, y, z)-1;
        if(l == -1) continue;
        if(!labels.atXYZ(x-1, y, z) &&
                surface_points[l]++ == *selected_points_i[l])
        {
            add_point(out, x, y, z, x-1, y, z);
            selected_points_i[l]++;
        }
        if(!labels.atXYZ(x+1, y, z) &&
                surface_points[l]++ == *selected_points_i[l])
        {
            add_point(out, x, y, z, x+1, y, z);
            selected_points_i[l]++;
        }
        if(!labels.atXYZ(x, y-1, z) &&
                surface_points[l]++ == *selected_points_i[l])
        {
            add_point(out, x, y, z, x, y-1, z);
            selected_points_i[l]++;
        }
        if(!labels.atXYZ(x, y+1, z) &&
                surface_points[l]++ == *selected_points_i[l])
        {
            add_point(out, x, y, z, x, y+1, z);
            selected_points_i[l]++;
        }
        if(!labels.atXYZ(x, y, z-1) &&
                surface_points[l]++ == *selected_points_i[l])
        {
            add_point(out, x, y, z, x, y, z-1);
            selected_points_i[l]++;
        }
        if(!labels.atXYZ(x, y, z+1) &&
                surface_points[l]++ == *selected_points_i[l])
        {
            add_point(out, x, y, z, x, y, z+1);
            selected_points_i[l]++;
        }
    }
    return out;
}

#endif

