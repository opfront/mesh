#ifndef __CIMG_SERIES_H__
#define __CIMG_SERIES_H__

#include <iomanip>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

template<class CIMG_TYPE>
bool
cimg_load_series(cimg_library::CImg<CIMG_TYPE> &image, const char *path,
    const char *filter)
{
    if(std::string(filter) == "")
    {
        image.load(path);
        return true;
    }
    namespace bf = boost::filesystem;
    const boost::regex regex_filter(filter);
    std::set<std::string> matching_files;
    for(bf::directory_iterator i(path); i != bf::directory_iterator(); i++)
    {
        if(!bf::is_regular_file(i->status())) continue;
        if(!boost::regex_match(i->path().filename().string(), regex_filter))
            continue;
        matching_files.insert(i->path().string());
    }
    if(!matching_files.size()) return false;
    std::set<std::string>::iterator i = matching_files.begin();
    std::cout << std::endl << "  " << i->c_str() << std::endl;
    cimg_library::CImg<CIMG_TYPE> slice(i->c_str());
    assert(slice.depth() == 1);
    image.assign(slice.width(), slice.height(), matching_files.size());
    cimg_forXY(slice, x, y) image(x, y, 0) = slice(x, y, 0);
    for(int z = 1; z < image.depth(); z++)
    {
        std::cout << "  " << (++i)->c_str() << std::endl;
        slice.load(i->c_str());
        assert(slice.width() == image.width() &&
               slice.height() == image.height() &&
               slice.depth() == 1);
        cimg_forXY(slice, x, y) image(x, y, z) = slice(x, y, 0);
    }
    return true;
}

template<class CIMG_TYPE>
void
cimg_save_series(cimg_library::CImg<CIMG_TYPE> &image, const char *prefix,
    const char *format)
{
    if(std::string(format) == "")
    {
        image.save(prefix);
    }
    std::stringstream a;
    a << image.depth()-1;
    int pad = a.str().length();
    cimg_library::CImg<CIMG_TYPE> slice(image.width(), image.height(), 1); 
    for(int z = 0; z < image.depth(); z++)
    {
        std::stringstream ss;
        ss << prefix << std::setfill('0') << std::setw(pad) << z << "." <<
            format;
        cimg_forXY(slice, x, y)
        {
            slice(x, y, 0) = image(x, y, z);
        }
        slice.save(ss.str().c_str());
    }
}


#endif

