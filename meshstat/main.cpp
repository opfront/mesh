#include <iostream>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include "util/mesh.h"
#include "util/off.h"
#include "util/lalg.h"
#include "util/vstat.h"

namespace po = boost::program_options;
namespace fs = boost::filesystem;

float *vertices;
unsigned vertex_count, triangle_count, *triangles;

float*
vertex(int t, int v)
{
    return &vertices[3*triangles[t*3+v]];
}

double
angle(float *v0, float *v1, float *v2)
{
    float d0[3], d1[3];
    linalg_sub_ve3_ve3(v1, v0, d0);
    linalg_sub_ve3_ve3(v2, v0, d1);
    return linalg_ang_ve3(d0, d1)/(3.14159265359)*180;
}

void
output(const char *filename, const std::vector<float> &values)
{
    std::ofstream file(filename);
    for(int i = 0; i < values.size(); i++) file << values[i] << std::endl;
}

void
angle_stat()
{
    std::vector<float> angles;
    for(int i = 0; i < triangle_count; i++)
    {
        angles.push_back(angle(vertex(i, 0), vertex(i, 1), vertex(i, 2)));
        angles.push_back(angle(vertex(i, 1), vertex(i, 0), vertex(i, 2)));
        angles.push_back(angle(vertex(i, 2), vertex(i, 0), vertex(i, 1)));
        int c = angles.size()-1;
    }
    output("angles.csv", angles);
    std::cout << "Triangle angles: " << vstat::summary(angles) << std::endl;
}

void
length_stat()
{
    std::vector<float> lengths;
    for(int i = 0; i < triangle_count; i++)
    {
        lengths.push_back(linalg_dis_ve3(vertex(i, 0), vertex(i, 1)));
        lengths.push_back(linalg_dis_ve3(vertex(i, 0), vertex(i, 2)));
        lengths.push_back(linalg_dis_ve3(vertex(i, 1), vertex(i, 2)));
    }
    output("lengths.csv", lengths);
    std::cout << "Edge lengths: " << vstat::summary(lengths) << std::endl;
}

int
main(int ac, char* av[])
{
    std::string image_filename, mesh_filename;
    try
    {
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("mesh_filename,m",
                po::value<std::string>(&mesh_filename),
                "surface or volumetric mesh filename (off or mesh format)");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help") || mesh_filename == "")
        {
            std::cout << "Output mesh statistics" << std::endl;
            std::cout << "Version 1.00 2014 Jens Petersen" << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        return 1;
    }
    fs::path mesh_path(mesh_filename);
    if(mesh_path.extension() == ".off")
    {
        if(!off_load(mesh_filename.c_str(), vertex_count, vertices,
                     triangle_count, triangles)) return 1;
    }
    else if(mesh_path.extension() == ".mesh")
    {
        if(!mesh_load(mesh_filename.c_str(), vertex_count, vertices,
                      triangle_count, triangles)) return 1;
    }
    else
    {
        std::cerr << "ERROR: Unknown extension!" << std::endl;
        return 1;
    }
    std::cout << "Vertices: " << vertex_count
        << " triangles: " << triangle_count << std::endl;
    angle_stat();
    length_stat();
    delete[] vertices;
    delete[] triangles;
    return 0;
}

