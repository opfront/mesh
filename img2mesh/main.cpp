#include <iostream>
#include <boost/program_options.hpp>
#include "img2mesh.h"

namespace po = boost::program_options;

int
main(int ac, char* av[])
{
    try
    {
        bool perturb = false, lloyd = false, odt = false, exude = false,
            skip_triangles = false;
        int minimum_initial_points;
        double isovalue, facet_angle, facet_size, facet_distance,
            cell_radius_edge_ratio, cell_size, initial_points_ratio,
            sliver_bound, time_limit;
        std::string image_path, regex_filter, cell_size_path,
            cell_size_regex_filter, mesh_filename, datatype;
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("image_path,i", po::value<std::string>(&image_path),
                "input isosurface image path (supports cimg formats)")
            ("regex_filter,I", po::value<std::string>(&regex_filter),
                "regex filter to select slices (optional)")
            ("cell_size_path,x", po::value<std::string>(&cell_size_path),
                "input image with cell size bounds (optional, cimg formats)")
            ("cell_size_regex_filter,X",
                po::value<std::string>(&cell_size_regex_filter),
                "regex filter to select slices (optional)")
            ("mesh_filename,o", po::value<std::string>(&mesh_filename),
                "output volumetric mesh filename (mesh format)")
            ("isovalue,v", po::value<double>(&isovalue)->default_value(0.5),
                "voxel value of surface points (bigger assumed inside)")
            ("facet_angle,a",
                po::value<double>(&facet_angle)->default_value(30),
                "surface facet angle lower bound (degrees)")
            ("facet_size,r",
                po::value<double>(&facet_size)->default_value(1),
                "facet surface Delaunay ball radius upper bound")
            ("facet_distance,d",
                po::value<double>(&facet_distance)->default_value(0.2),
                "facet approximation error upper bound")
            ("cell_radius_edge_ratio,c",
                po::value<double>(&cell_radius_edge_ratio)->default_value(2),
                "tetrahedron circumradius and edge length ratio upper bound")
            ("cell_size,s",
                po::value<double>(&cell_size)->default_value(1000),
                "tetrahedron circumradius upper bound (0 <= unlimited)")
            ("sliver_bound,b",
                po::value<double>(&sliver_bound)->default_value(0),
                "lower bound on dihedral angles (exude and perturb options)")
            ("minimum_initial_points,m",
                po::value<int>(&minimum_initial_points)->default_value(8),
                "minimum number of initial points on each component")
            ("initial_points_ratio,p",
                po::value<double>(&initial_points_ratio)->default_value(0.1),
                "ratio of initial points to surface voxel facets")
            ("time_limit,l",
                po::value<double>(&time_limit)->default_value(0),
                "time limit (s) of perturb, lloyd, odt, and exude operations")
            ("perturb", po::value<bool>(&perturb)->multitoken()->zero_tokens(),
                "optimize using CGAL's perturb function")
            ("lloyd", po::value<bool>(&lloyd)->multitoken()->zero_tokens(),
                "optimize using Lloyd's algorithm")
            ("odt", po::value<bool>(&odt)->multitoken()->zero_tokens(),
                "optimize using Odt-smoothing")
            ("exude", po::value<bool>(&exude)->multitoken()->zero_tokens(),
                "optimize using CGAL's exude function")
            ("datatype,t", po::value<std::string>(&datatype)->
                default_value("float"), "uchar, short, float, double")
            ("skip_triangles,T",
                po::value<bool>(&skip_triangles)->multitoken()->zero_tokens(),
                "do not output triangles");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help") || image_path == "" || mesh_filename == "")
        {
            std::cout << "Volumetric mesh from isosurface image" << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
        if(datatype == "uchar")
        {
            img2mesh<unsigned char, float>(image_path.c_str(),
                regex_filter.c_str(), mesh_filename.c_str(), isovalue,
                facet_angle, facet_size, facet_distance,
                cell_radius_edge_ratio, cell_size, cell_size_path.c_str(),
                cell_size_regex_filter.c_str(), sliver_bound,
                minimum_initial_points, initial_points_ratio, time_limit,
                perturb, lloyd, odt, exude, skip_triangles);
        }
        else if(datatype == "short")
        {
            img2mesh<short, float>(image_path.c_str(),
                regex_filter.c_str(), mesh_filename.c_str(), isovalue,
                facet_angle, facet_size, facet_distance,
                cell_radius_edge_ratio, cell_size, cell_size_path.c_str(),
                cell_size_regex_filter.c_str(), sliver_bound,
                minimum_initial_points, initial_points_ratio, time_limit,
                perturb, lloyd, odt, exude, skip_triangles);
        }
        else if(datatype == "float")
        {
            img2mesh<float, float>(image_path.c_str(),
                regex_filter.c_str(), mesh_filename.c_str(), isovalue,
                facet_angle, facet_size, facet_distance,
                cell_radius_edge_ratio, cell_size, cell_size_path.c_str(),
                cell_size_regex_filter.c_str(), sliver_bound,
                minimum_initial_points, initial_points_ratio, time_limit,
                perturb, lloyd, odt, exude, skip_triangles);
        }
        else if(datatype == "double")
        {
            img2mesh<double, float>(image_path.c_str(),
                regex_filter.c_str(), mesh_filename.c_str(), isovalue,
                facet_angle, facet_size, facet_distance,
                cell_radius_edge_ratio, cell_size, cell_size_path.c_str(),
                cell_size_regex_filter.c_str(), sliver_bound,
                minimum_initial_points, initial_points_ratio, time_limit,
                perturb, lloyd, odt, exude, skip_triangles);
        }
        else
        {
            std::cerr << "ERROR: Unsupported datatype!" << std::endl;
            return 1;
        }
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}

