#ifndef __IMG2MESH_H__
#define __IMG2MESH_H__

#include <iostream>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>
#include <CGAL/make_mesh_3.h>
#include "cimgext/cimgext.h"
#include "util/file_medit.h"
#include "util/cimg_series.h"

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::FT field_type;
typedef K::Point_3 point_type;
typedef K::Sphere_3 sphere_type;

#include "util/meshtraits.h"
#include "util/meshdomain.h"
#include "util/cgalmesh.h"

template<class CIMG_TYPE>
class sizing_field
{
public:
    typedef field_type FT;
    typedef point_type Point_3;
    typedef mesh_domain<function_type, K, CIMG_TYPE> mesh_domain_type;
    typedef typename mesh_domain_type::Index Index;
    cimg_library::CImg<CIMG_TYPE> image;

    FT operator()(const Point_3& p, const int, const Index&) const
    {
        return image.cubic_atXYZ(p[0], p[1], p[2]);
    }
};

template<class CIMG_TYPE, class FLOAT_TYPE>
int
img2mesh(const char *image_path,
    const char *regex_filter,
    const char *mesh_filename,
    FLOAT_TYPE isovalue,
    FLOAT_TYPE facet_angle,
    FLOAT_TYPE facet_size,
    FLOAT_TYPE facet_distance,
    FLOAT_TYPE cell_radius_edge_ratio,
    FLOAT_TYPE cell_size,
    const char *cell_size_path,
    const char *cell_size_regex_filter,
    FLOAT_TYPE sliver_bound,
    int minimum_initial_points,
    FLOAT_TYPE initial_points_ratio,
    FLOAT_TYPE time_limit,
    bool perturb,
    bool lloyd,
    bool odt,
    bool exude,
    bool skip_triangles)
{
    typedef mesh_domain<function_type, K, CIMG_TYPE> mesh_domain_type;
    typedef typename CGAL::Mesh_triangulation_3<mesh_domain_type>::type Tr;
    typedef CGAL::Mesh_complex_3_in_triangulation_3<Tr> C3t3;
    typedef CGAL::Mesh_criteria_3<Tr> Mesh_criteria;
    if(facet_angle < 0)
    {
        std::cerr << "ERROR: facet_angle < 0!" << std::endl;
        return 1;
    }
    if(facet_size < 0)
    {
        std::cerr << "ERROR: facet_size < 0!" << std::endl;
        return 1;
    }
    if(facet_distance < 0)
    {
        std::cerr << "ERROR: facet_distance < 0!" << std::endl;
        return 1;
    }
    if(cell_radius_edge_ratio < 0)
    {
        std::cerr << "ERROR: cell_radius_edge_ratio < 0!" << std::endl;
        return 1;
    }
    if(cell_size <= 0)
    {
        std::cerr << "ERROR: cell_size <= 0!" << std::endl;
        return 1;
    }
    if(facet_angle > 30)
    {
        std::cout << "WARNING: not guaranteed to terminate with "
            "facet_angle > 30!" << std::endl;
    }
    if(cell_radius_edge_ratio < 2)
    {
        std::cout << "WARNING: not guaranteed to terminate with "
            "cell_radius_edge_ratio < 2!" << std::endl;
    }
    std::cout << "Loading image(s): " << image_path << " " << regex_filter
        << std::flush;
    cimg_library::CImg<CIMG_TYPE> image;
    if(!cimg_load_series(image, image_path, regex_filter))
    {
        std::cerr << "ERROR: could not load file(s)!" << std::endl;
        return 1;
    }
    std::cout << " done!" << std::endl;
    std::cout << "Generating CGAL mesh... " << std::flush;
    image -= isovalue;
    sphere_type bs(bounding_sphere(image));
    image = -image;
    CIMG_TYPE boundary = image.max();
    boundary_value = static_cast<void*>(&boundary);
    isosurface_image = static_cast<void*>(&image);
    // Note: uses facet_distance/100 as approximation error bound.
    // Since both values somehow bound mesh to isosurface distance they should
    // be similar. However, low values of approximation error are
    // computationally cheap, so we will compromise a bit.
    mesh_domain_type domain(isovalue_function<CIMG_TYPE>, &image, bs,
        facet_distance/100, minimum_initial_points, initial_points_ratio);
    sizing_field<CIMG_TYPE> cell_size_field;
    if(std::string(cell_size_path) == "")
    {
        cell_size_field.image.assign(image.width(), image.height(),
                                     image.depth(), 1, cell_size);
    }
    else if(!cimg_load_series(cell_size_field.image, cell_size_path, 
                         cell_size_regex_filter))
    {
        std::cerr << "ERROR: could not load file(s)!" << std::endl;
        return 1;
    }
    CIMG_TYPE min_val = std::numeric_limits<CIMG_TYPE>::max();
    cimg_forXYZ(cell_size_field.image, x, y, z)
    {
        if(cell_size_field.image(x, y, z) > 0 &&
           cell_size_field.image(x, y, z) < min_val)
            min_val = cell_size_field.image(x, y, z);
    }
    cimg_forXYZ(cell_size_field.image, x, y, z)
    {
        if(cell_size_field.image(x, y, z) <= 0)
            cell_size_field.image(x, y, z) = min_val;
    }
    Mesh_criteria criteria(CGAL::parameters::facet_angle = facet_angle,
        CGAL::parameters::facet_size = facet_size,
        CGAL::parameters::facet_distance = facet_distance,
        CGAL::parameters::cell_radius_edge_ratio = cell_radius_edge_ratio,
        CGAL::parameters::cell_size = cell_size_field);
    C3t3 c3t3 = CGAL::make_mesh_3<C3t3>(domain, criteria,
        CGAL::parameters::no_perturb(),
        CGAL::parameters::no_exude());
    std::cout << "Done!" << std::endl;
    if(perturb)
    {
        std::cout << "Perturbing... " << std::flush;
        std::cout << mesh_return_code(CGAL::perturb_mesh_3(c3t3, domain,
            CGAL::parameters::sliver_bound = sliver_bound,
            CGAL::parameters::time_limit = time_limit)) << std::endl;
    }
    if(lloyd)
    {
        std::cout << "Lloyd optimizing... " << std::flush;
        std::cout << mesh_return_code(CGAL::lloyd_optimize_mesh_3(c3t3, domain,
            CGAL::parameters::time_limit = time_limit,
            CGAL::parameters::do_freeze = false)) << std::endl;
    }
    if(odt)
    {
        std::cout << "Odt-smoothing... " << std::flush;
        std::cout << mesh_return_code(CGAL::odt_optimize_mesh_3(c3t3, domain,
            CGAL::parameters::time_limit = time_limit)) << std::endl;
    }
    if(exude)
    {
        std::cout << "Exuding... " << std::flush;
        std::cout << mesh_return_code(CGAL::exude_mesh_3(c3t3,
            CGAL::parameters::sliver_bound = sliver_bound,
            CGAL::parameters::time_limit = time_limit)) << std::endl;
    }
    std::cout << "Saving... " << std::flush;
    std::ofstream medit_file(mesh_filename);
    if(skip_triangles) output_to_medit_no_triangles(medit_file, c3t3);
    else output_to_medit(medit_file, c3t3, false, true);
    //c3t3.output_to_medit(medit_file);
    std::cout << "done!" << std::endl;
    return 0;
}


#endif

