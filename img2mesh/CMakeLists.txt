add_executable(img2mesh main.cpp)

target_link_libraries(img2mesh ${Boost_LIBRARIES})
target_link_libraries(img2mesh ${CImg_SYSTEM_LIBS})
target_link_libraries(img2mesh ${CGAL_LIBRARY})
target_link_libraries(img2mesh ${CGAL_Core_LIBRARY})
target_link_libraries(img2mesh ${GMP_LIBRARIES})
target_link_libraries(img2mesh ${MPFR_LIBRARIES})

if(NiftiCLib_FOUND)
    target_link_libraries(img2mesh ${NiftiCLib_LIBRARIES})
endif(NiftiCLib_FOUND)

install(TARGETS img2mesh DESTINATION bin)
