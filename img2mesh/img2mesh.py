import argparse
from mesher import img2mesh_c

def main():
    parser = argparse.ArgumentParser(
        formatter_class = argparse.ArgumentDefaultsHelpFormatter,
        description = 'Volumetric mesh from isosurface image')
    parser.add_argument('-i',
        help = 'input isosurface image path (cimg supported format)',
        required = True, dest = 'image_path')
    parser.add_argument('-I',
        help = 'regex filter to select slices',
        required = False, dest = 'regex_filter')
    parser.add_argument('-o',
        help = 'output volumetric mesh filename (mesh format)',
        required = True, dest = 'mesh_filename')
    parser.add_argument('-v',
        help = 'voxel value of surface points (bigger assumed inside)',
        default = 0.5, type = float, dest = 'isovalue')
    parser.add_argument('-a',
        help = 'surface facet angle lower bound (degrees)',
        default = 30, type = float, dest = 'facet_angle')
    parser.add_argument('-r',
        help = 'facet surface Delaunay ball radius upper bound',
        default = 1, type = float, dest = 'facet_size')
    parser.add_argument('-d',
        help = 'facet approximation error upper bound',
        default = 1, type = float, dest = 'facet_distance')
    parser.add_argument('-c',
        help = 'tetrahedron circumradius and edge length ratio upper bound',
        default = 2, type = float, dest = 'cell_radius_edge_ratio')
    parser.add_argument('-s',
        help = 'tetrahedron circumradius upper bound',
        default = 1, type = float, dest = 'cell_size')
    parser.add_argument('-b',
        help = 'lower bound on dihedral angles (exude and perturb options)',
        default = 0, type = float, dest = 'sliver_bound')
    parser.add_argument('-m',
        help = 'minimum number of initial points on each component',
        default = 10, type = int, dest = 'minimum_initial_points')
    parser.add_argument('-p',
        help = 'ratio of initial points to surface voxel facets',
        default = 0, type = float, dest = 'initial_points_ratio')
    parser.add_argument('-l',
        help = 'time limit (s) of perturb, lloyd, odt, and exude operations',
        default = 0, type = float, dest = 'time_limit')
    parser.add_argument('--perturb',
        help = "optimize using CGAL's perturb function",
        dest = 'perturb', action = 'store_true')
    parser.add_argument('--lloyd',
        help = "optimize using Lloyd's algorithm",
        dest = 'lloyd', action = 'store_true')
    parser.add_argument('--odt',
        help = "optimize using ODT-smoothing",
        dest = 'odt', action = 'store_true')
    parser.add_argument('--exude',
        help = "optimize using CGAL's exude function",
        dest = 'exude', action = 'store_true')
    parser.add_argument('-T',
        help = "Do not output triangles",
        dest = 'skip_triangles', action = "store_true") 
    options = parser.parse_args()
    img2mesh_c(options.image_path, options.regex_filter, options.mesh_filename,
        options.isovalue, options.facet_angle, options.facet_size,
        options.facet_distance, options.cell_radius_edge_ratio,
        options.cell_size, options.sliver_bound,
        options.minimum_initial_points, options.initial_points_ratio,
        options.time_limit, options.perturb, options.lloyd, options.odt,
        options.exude, options.skip_triangles)

if __name__ == "__main__":
    main()


