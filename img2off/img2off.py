import argparse
from mesher import img2off_c

def main():
    parser = argparse.ArgumentParser(
        formatter_class = argparse.ArgumentDefaultsHelpFormatter,
        description = '3d surface mesh from isosurface image')
    parser.add_argument('-i',
        help = 'input isosurface image path (cimg supported format)',
        required = True, dest = 'image_path')
    parser.add_argument('-I',
        help = 'regex filter to select slices',
        required = False, dest = 'regex_filter')
    parser.add_argument('-o',
        help = 'output surface mesh filename (off format)',
        required = True, dest = 'mesh_filename')
    parser.add_argument('-v',
        help = 'voxel value of surface points (bigger assumed inside)',
        default = 0.5, type = float, dest = 'isovalue')
    parser.add_argument('-a', default = 30, type = float,
        help = 'facet minimum angle (degrees)', dest = 'angle')
    parser.add_argument('-r', default = 1, type = float,
        help = 'delaunay radius bound', dest = 'radius')
    parser.add_argument('-d', default = 1, type = float,
        help = 'facets center-center distance bound', dest = 'distance')
    parser.add_argument('-e', default = 10e-5, type = float,
        help = 'isovalue error bound', dest = 'error')
    parser.add_argument('-m', default = 10, type = int,
        dest = 'minimum_initial_points',
        help = 'minimum number of initial points on each component')
    parser.add_argument('-p',
        help = 'ratio of initial points to surface voxel facets',
        default = 0, type = float, dest = 'initial_points_ratio')
    options = parser.parse_args()
  
    img2off_c(options.image_path, options.regex_filter, options.mesh_filename,
        options.isovalue, options.angle, options.radius, options.distance,
        options.error, options.minimum_initial_points,
        options.initial_points_ratio)

if __name__ == "__main__":
    main()
