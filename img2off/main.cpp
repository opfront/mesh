#include <iostream>
#include <boost/program_options.hpp>
#include "img2off.h"

namespace po = boost::program_options;

int
main(int ac, char* av[])
{
    try
    {
        int minimum_initial_points;
        double isovalue, angle, radius, distance, error, initial_points_ratio;
        std::string image_path, regex_filter, mesh_filename, datatype;
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("image_path,i", po::value<std::string>(&image_path),
                "input isosurface image path (cimg supported format)")
             ("regex_filter,I", po::value<std::string>(&regex_filter),
                "regex filter to select slices (optional)")
            ("mesh_filename,o", po::value<std::string>(&mesh_filename),
                "output surface mesh filename (off format)")
            ("isovalue,v", po::value<double>(&isovalue)->default_value(0.5),
                "voxel value of surface points (bigger assumed inside)")
            ("angle,a", po::value<double>(&angle)->default_value(30),
                "facet minimum angle (degrees)")
            ("radius,r", po::value<double>(&radius)->default_value(1),
                "delaunay radius bound")
            ("distance,d", po::value<double>(&distance)->default_value(0.1),
                "facets center-center distance bound")
            ("error,e", po::value<double>(&error)->default_value(0.0001),
                "isovalue error bound")
            ("minimum_initial_points,m",
                po::value<int>(&minimum_initial_points)->default_value(8),
                "minimum number of initial points on each component")
            ("initial_points_ratio,p",
                po::value<double>(&initial_points_ratio)->default_value(0.1),
                "ratio of initial points to surface voxel facets")
            ("datatype,t", po::value<std::string>(&datatype)->
                default_value("float"), "uchar, short, float, double");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help") || image_path == "" || mesh_filename == "")
        {
            std::cout << "3d surface mesh from isosurface image" << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
        if(datatype == "uchar")
        {
            img2off<unsigned char>(image_path.c_str(),
                regex_filter.c_str(), mesh_filename.c_str(), isovalue, angle,
                radius, distance, error, minimum_initial_points,
                initial_points_ratio);
        }
        else if(datatype == "short")
        {
            img2off<short>(image_path.c_str(),
                regex_filter.c_str(), mesh_filename.c_str(), isovalue, angle,
                radius, distance, error, minimum_initial_points,
                initial_points_ratio);
        }
        else if(datatype == "float")
        {
            img2off<float>(image_path.c_str(),
                regex_filter.c_str(), mesh_filename.c_str(), isovalue, angle,
                radius, distance, error, minimum_initial_points,
                initial_points_ratio);
        }
        else if(datatype == "double")
        {
            img2off<double>(image_path.c_str(),
                regex_filter.c_str(), mesh_filename.c_str(), isovalue, angle,
                radius, distance, error, minimum_initial_points,
                initial_points_ratio);
        }
        else
        {
            std::cerr << "ERROR: Unsupported datatype!" << std::endl;
            return 1;
        }
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}

