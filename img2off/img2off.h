#ifndef __IMG2OFF_H__
#define __IMG2OFF_H__

#include <iostream>
#include <CGAL/Surface_mesh_default_triangulation_3.h>
#include <CGAL/Surface_mesh_default_criteria_3.h>
#include <CGAL/Complex_2_in_triangulation_3.h>
#include <CGAL/IO/Complex_2_in_triangulation_3_file_writer.h>
#include <fstream>
#include <CGAL/make_surface_mesh.h>
#include <CGAL/Gray_level_image_3.h>
#include <CGAL/Implicit_surface_3.h>
#include "cimgext/cimgext.h"
#include "util/cimg_series.h"

typedef CGAL::Surface_mesh_default_triangulation_3 triangulation_type;
typedef CGAL::Complex_2_in_triangulation_3<triangulation_type> complex_type;
typedef triangulation_type::Geom_traits geom_traits_type;
typedef geom_traits_type::Point_3 point_type;
typedef geom_traits_type::Sphere_3 sphere_type;
typedef geom_traits_type::FT field_type;

#include "util/meshtraits.h"
#include "util/cgalmesh.h"

typedef CGAL::Implicit_surface_3<geom_traits_type, function_type> surface_type;

template<class CIMG_TYPE>
int
img2off(const char *image_path, const char *regex_filter,
    const char *mesh_filename, double isovalue, double angle, double radius,
    double distance, double error, int minimum_initial_points,
    double initial_points_ratio)
{
    if(distance <= 0)
    {
        std::cerr << "ERROR: distance <= 0!" << std::endl;
        return 1;
    }
    if(radius <= 0)
    {
        std::cerr << "ERROR: radius <= 0!" << std::endl;
        return 1;
    }
    if(error <= 0)
    {
        std::cerr << "ERROR: error <= 0!" << std::endl;
        return 1;
    }
    if(initial_points_ratio < 0 || initial_points_ratio > 1)
    {
        std::cerr << "ERROR: initial_points_ratio not between 0 and 1!"
            << std::endl;
        return 1;
    }
    if(angle > 30)
    {
        std::cout << "WARNING: not guaranteed to terminate with "
            "angle > 30!" << std::endl;
    }
    std::cout << "Loading image(s)" << image_path << " " << regex_filter
        << std::flush;
    cimg_library::CImg<CIMG_TYPE> image;
    if(!cimg_load_series(image, image_path, regex_filter))
    {
        std::cerr << "ERROR: could not load file(s)!" << std::endl;
        return 1;
    }
    std::cout << " done!" << std::endl;
    image -= isovalue;
    CIMG_TYPE boundary = image.min();
    boundary_value = static_cast<void*>(&boundary);
    isosurface_image = static_cast<void*>(&image);
    // 3d-Delaunay triangulation
    triangulation_type tr;
    // 2d-complex in 3D-Delaunay triangulation
    complex_type c2t3(tr);
    // Carefully choosen bounding sphere: the center must be inside the
    // surface defined by 'image' and the radius must be so large that
    // the sphere actually bounds the whole image.
    // Definition of the surface, with error as relative precision.
    surface_type surface(isovalue_function<CIMG_TYPE>, bounding_sphere(image),
        error);
    // Defining meshing criteria.
    std::cout << "Defining meshing criteria... " << std::flush;
    CGAL::Surface_mesh_default_criteria_3<triangulation_type>
        criteria(angle, radius, distance);
    surface_mesh_traits_type<surface_type, CIMG_TYPE>
        surface_mesh_traits(&image, minimum_initial_points,
        initial_points_ratio);
    std::cout << "done!" << std::endl;
    //surface_mesh_traits_type surface_mesh_traits();
    // Meshing surface, with the "manifold without boundary" algorithm.
    std::cout << "Generating CGAL mesh... " << std::flush;
    CGAL::make_surface_mesh(c2t3, surface, surface_mesh_traits, criteria,
        CGAL::Manifold_tag());
    std::cout << "done!" << std::endl;
    // Saving surface to off format.
    std::cout << "Saving... " << std::flush;
    std::ofstream out(mesh_filename);
    CGAL::output_surface_facets_to_off(out, c2t3);
    std::cout << " done!" << std::endl;
    return 0;
}

#endif

