#ifndef __MYCIMG_H__
#define __MYCIMG_H__

#include <typeinfo>
#include <assert.h>
#include <fstream>

#ifdef NIFTI
// Note we need to include this first to avoid twicely defined macros.
#include "dirent.h"

#include "nifti1_io.h"
#endif

#define cimgext_load(ext, func)\
    if(!cimg::strncasecmp(cimg::split_filename(filename), ext,strlen(ext)))\
        return func(filename);
#define cimgext_save(ext, func)\
    if(!cimg::strncasecmp(cimg::split_filename(fn), ext, strlen(ext)))\
        return func(fn);

#include "cimgext/cimgextload.h"
#include "cimgext/cimgextsave.h"

#define cimg_plugin "cimgext/cimgextincl.h"

#define cimg_display 0
#include "CImg.h"

#endif
