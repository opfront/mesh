public:
	struct
    {
		double origin[3], spacing[3];
	} metadata;

    template<typename IMGTYPE>
    void
    set_metadata(const IMGTYPE& img)
    {
        for(int i = 0; i < 3; i++)
        {
            metadata.origin[i] = img.metadata.origin[i];
            metadata.spacing[i] = img.metadata.spacing[i];
        }
    }
    
    template<class FLOAT_TYPE>
    void
    xyz2ijk(FLOAT_TYPE &x, FLOAT_TYPE &y, FLOAT_TYPE &z) const
    {
        assert(metadata.spacing[0] && metadata.spacing[1] &&
            metadata.spacing[2]);
        x = -(x+metadata.origin[0])/metadata.spacing[0];
        y = -(y+metadata.origin[1])/metadata.spacing[1];
        z = (z-metadata.origin[2])/metadata.spacing[2];
    }
    
    template<class FLOAT_TYPE>
    void
    xyz2ijk(FLOAT_TYPE *p) const
    {
        xyz2ijk(p[0], p[1], p[2]);
    }
    
    template<class FLOAT_TYPE>
    void
    ijk2xyz(FLOAT_TYPE &i, FLOAT_TYPE &j, FLOAT_TYPE &k) const
    {
        i = -metadata.spacing[0]*i-metadata.origin[0];
        j = -metadata.spacing[1]*j-metadata.origin[1];
        k = metadata.spacing[2]*k+metadata.origin[2];
    }

    template<class FLOAT_TYPE>
    void
    ijk2xyz(FLOAT_TYPE *p) const
    {
        ijk2xyz(p[0], p[1], p[2]);
    }
     
