
public:
    bool
    load_vol(const char *filename)
    {
        int x, y, z;
        double voxel_size;
        std::string header, temp;
        std::ifstream info((std::string(filename)+".info").c_str());
        if(!info.is_open()) return false;
        std::getline(info, header);
        //if(header != "! PyHST_SLAVE VOLUME INFO FILE") return false;
        info >> temp >> temp >> x >> temp >> temp >> y >> temp >> temp >> z
            >> temp >> temp >> voxel_size;
        std::getline(info, header);
        std::getline(info, header);
        if(header != "BYTEORDER = LOWBYTEFIRST") return false;
        assign(x, y, z);
        metadata.origin[0] = 0;
        metadata.origin[1] = 0;
        metadata.origin[2] = 0;
        metadata.spacing[0] = voxel_size;
        metadata.spacing[1] = voxel_size;
        metadata.spacing[2] = voxel_size;
        std::ifstream vol(filename, std::ios::in|std::ios::binary);
        if(!vol.is_open()) return false;
        for(int k = 0; k < z; k++)
            for(int j = 0; j < y; j++)
                for(int i = 0; i < x; i++)
        {
            char buf[sizeof(T)];
            vol.read(buf, sizeof(T));
            (*this)(i, j, k) = *((T*)buf);
        }
        return true;
    }

    bool
    save_vol(const char *filename) const
    {
        std::ofstream info((std::string(filename)+".info").c_str());
        if(!info.is_open()) return false;
        info << "! PyHST_SLAVE VOLUME INFO FILE" << std::endl
            << "NUM_X =  " << this->width() << std::endl << "NUM_Y =  "
            << this->height() << std::endl << "NUM_Z =  " << this->depth()
            << std::endl << "voxelSize =  " << metadata.spacing[0]
            << std::endl << "BYTEORDER = LOWBYTEFIRST" << std::endl;
        std::ofstream vol(filename, std::ios::out|std::ios::binary);
        if(!vol.is_open()) return false;
        for(int k = 0; k < depth(); k++)
            for(int j = 0; j < height(); j++)
                for(int i = 0; i < width(); i++)
        {
            char *buf = (char*)&(*this)(i, j, k);
            vol.write(buf, sizeof(T));
        }
        return true;
    }

    CImg&
    load_plugin_vol(const char *filename)
    {
        if(!load_vol(filename))
        {
            throw CImgIOException(
                "CImg<%s>::load_plugin_vol() : Error loading %s!",
                pixel_type(), filename);
        }
        return *this;
    }

    const CImg&
    save_plugin_vol(const char *filename) const
    {
        if(!save_vol(filename))
        {
            throw CImgIOException(
                "CImg<%s>::save_plugin_vol() : Error saving %s!",
                pixel_type(), filename);
        }
        return *this;
    }

