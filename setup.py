from distutils.core import setup, Extension

module1 = Extension('mesher',
                    include_dirs = ['.'],
                    extra_compile_args=['-DNIFTI'],
                    libraries = ['niftiio', 'znz', 'z', 'CGAL', 'gmp', 'mpfr',
                                 'boost_filesystem', 'boost_regex'],
                    sources = ['meshermodule.cpp'])

setup(ext_modules = [module1])
