#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include "cimgext/cimgext.h"
#include "util/mesh.h"
#include "util/off.h"
#include "util/lalg.h"
#include "util/cimg_series.h"

namespace po = boost::program_options;
namespace fs = boost::filesystem;

float view_x_min = -1, view_x_max = 1, view_y_min = -1, view_y_max = 1;
int window_height, window_width, image_slice;
bool wireframe = true;
float *vertices;
unsigned vertex_count, triangle_count, *triangles;
std::vector<std::vector<float> > lines;
    
cimg_library::CImg<float> image, slice;

void
lines_add(float *v)
{
    for(int i = 0; i < 3; i++) lines[image_slice].push_back(v[i]);
}

float*
vertex(int t, int v)
{
    return &vertices[3*triangles[t*3+v]];
}

// Compute intersection point p, of edge i-j in triangle t with viewed slice.
//  Returns false if none.
bool
edge_intersection(int t, int i, int j, float *p)
{
    double slice_pos = static_cast<double>(image_slice)/(image.depth()-1)*2-1;
    const float *v = vertex(t, i), *u = vertex(t, j);
    if(u[2] < slice_pos && v[2] < slice_pos) return false;
    if(u[2] > slice_pos && v[2] > slice_pos) return false;
    // Ignore parallel case for now!
    if(u[2] == slice_pos && v[2] == slice_pos) return false;
    
    linalg_cop_ve3(v, p);
    linalg_sub_ve3_ve3(u, p);
    float z = fabs(slice_pos-u[2])/fabs(v[2]-u[2]);
    linalg_mul_scl_ve3(z, p);
    linalg_add_ve3_ve3(u, p);
    assert(fabs(p[2]-slice_pos) < 0.0001);
    return true;
}

void
init_slice()
{
    std::cout << "Drawing slice " << image_slice << " out of " << image.depth()
        << std::endl;
    cimg_forXYZ(slice, x, y, z)
    {
        if(y >= image.width()) continue;
        if(z >= image.height()) continue;
        if(image.spectrum() > 1)
            slice(x, y, z) = image(y, z, image_slice, x);
        else
            slice(x, y, z) = image(y, z, image_slice);
    }
    GLuint object;
    glGenTextures(1, &object);
    glBindTexture(GL_TEXTURE_2D, object);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, slice.height(), slice.depth(), 0,
        GL_RGB, GL_FLOAT, slice.data());
    if(lines[image_slice].size()) return;
    for(int i = 0; i < triangle_count; i++)
    {
        bool t0, t1, t2;
        float p0[3], p1[3], p2[3];
        t0 = edge_intersection(i, 0, 1, p0);
        t1 = edge_intersection(i, 0, 2, p1);
        t2 = edge_intersection(i, 1, 2, p2);
        int b = lines[image_slice].size();
        if(t0 && t1)
        {
            lines_add(p0);
            lines_add(p1);
        }
        if(t0 && t2)
        {
            lines_add(p0);
            lines_add(p2);
        }
        if(t1 && t2)
        {
            lines_add(p1);
            lines_add(p2);
        }
    }
}

void
update_view_dimensions(float x, float y, float f)
{
    float span_x = view_x_max-view_x_min,
        span_y = view_y_max-view_y_min;
    x = view_x_min+x/window_width*span_x;
    y = view_y_min+y/window_height*span_y;
    span_x *= f;
    span_y *= f;
    if(span_x > 2) span_x = 2;
    else if(span_x < 0.0001) span_x = 0.0001;
    if(span_y > 2) span_y = 2;
    else if(span_y < 0.0001) span_y = 0.0001;
    view_x_min = x-0.5*span_x;
    view_x_max = x+0.5*span_x;
    view_y_min = y-0.5*span_y;
    view_y_max = y+0.5*span_y;
    // Move view into bounds.
    if(view_x_min < -1)
    {
        view_x_max -= view_x_min+1;
        view_x_min = -1;
    }
    if(view_x_max > 1)
    {
        view_x_min -= view_x_max-1;
        view_x_max = 1;
    }
    if(view_y_min < -1)
    {
        view_y_max -= view_y_min+1;
        view_y_min = -1;
    }
    if(view_y_max > 1)
    {
        view_y_min -= view_y_max-1;
        view_y_max = 1;
    }
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(view_x_min, view_x_max, view_y_max, view_y_min, -1.0, 1.0);
}

void
zoom_in(float x, float y)
{
    update_view_dimensions(x, y, 0.5);
}

void
zoom_out(float x, float y)
{
    update_view_dimensions(x, y, 2);
}

void
update()
{
    double e = 1+1.0/(image.width()),
        f = 1+1.0/(image.height()),
        w = static_cast<double>(image.width())/(slice.height()),
        h = static_cast<double>(image.height())/(slice.depth());
    glClear(GL_COLOR_BUFFER_BIT);
    //glBindTexture(GL_TEXTURE_2D, tex);
    glEnable(GL_TEXTURE_2D);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glColor3f(1.0, 1.0, 1.0);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f(-e, f, 0);
    glTexCoord2f(w, 0);
    glVertex3f(e, f, 0);
    glTexCoord2f(w, h);
    glVertex3f(e, -f, 0);
    glTexCoord2f(0, h);
    glVertex3f(-e, -f, 0);
    glEnd();
    glDisable(GL_TEXTURE_2D);
    glLineWidth(4);
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &lines[image_slice][0]);
    glColor3f(1.0f, 0.3f, 0.3f);
    if(wireframe) glDrawArrays(GL_LINES, 0, lines[image_slice].size()/3);
    SDL_GL_SwapBuffers();
}

void
init_gl(int ac, char* av[])
{
    SDL_Init(SDL_INIT_VIDEO);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_Surface* screen = SDL_SetVideoMode(window_width, window_height, 16,
        SDL_OPENGL);    
    glDisable(GL_CULL_FACE);
    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
    glShadeModel(GL_SMOOTH);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(view_x_min, view_x_max, view_y_max, view_y_min, -1.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glVertexPointer(3, GL_FLOAT, 0, vertices);
    //update_view_dimensions(0.5, 0.5);
    init_slice();
}

void
DisplayKeyDown(SDL_KeyboardEvent *key)
{
    switch(key->keysym.sym)
    {
    case 'k':
        wireframe = !wireframe;
        if(wireframe) std::cout << "Drawing wireframe" << std::endl;
        else std::cout << "Not drawing wireframe" << std::endl;
        init_slice();
        break;
    case 'w':
        if(++image_slice >= image.depth()) image_slice = image.depth()-1;
        init_slice();
        break;
    case 's':
        if(--image_slice < 0) image_slice = 0;
        init_slice();
        break;
    case 27:
        exit(0);
        break;
    }
}

void
DisplayKeyUp(SDL_KeyboardEvent *key)
{
    switch(key->keysym.sym)
    {
    default:
        break;
    }
}

void
main_loop()
{
    SDL_Event event;
    int running = 1;

    int cur_time = SDL_GetTicks(), last_time = 0, k = 0, delta = 0;
    while(running)
    {
        if(!(++k%100))
        {
            delta = cur_time-last_time;
            last_time = cur_time;
            k = 0;
        }
        cur_time = SDL_GetTicks();
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
            case SDL_KEYDOWN:
                DisplayKeyDown(&event.key);
                break;
            case SDL_KEYUP:
                DisplayKeyUp(&event.key);
                break;
            case SDL_MOUSEBUTTONDOWN:
                if(event.button.button == SDL_BUTTON_LEFT)
                {
                    std::cout << "Zoom in " << event.button.x << " "
                        << event.button.y << std::endl;
                    zoom_in(event.button.x, event.button.y);
                }
                else if(event.button.button == SDL_BUTTON_RIGHT)
                {
                    std::cout << "Zoom out " << std::endl;
                    zoom_out(event.button.x, event.button.y);
                }
                break;
            case SDL_QUIT:
                running = 0;
                break;
            }
        }
        if(delta == 0) delta = -1;
        std::cout << cur_time << " FPS: " << 100000/delta
                  << "     \r" << std::flush;
        update();
    }
}

int
main(int ac, char* av[])
{
#ifdef WINDOWS_CONSOLE
    std::ofstream ctt("CON");
    freopen("CON", "w", stdout);
    freopen("CON", "w", stderr);
#endif
    std::string image_path, regex_filter, mesh_filename;
    try
    {
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("image_path,i", po::value<std::string>(&image_path),
                "input image path (supports cimg formats)")
            ("regex_filter,I", po::value<std::string>(&regex_filter),
                "regex filter to select slices (optional)")
            ("mesh_filename,m",
                po::value<std::string>(&mesh_filename),
                "surface or volumetric mesh filename (off or mesh format)")
            ("window_height,H",
                po::value<int>(&window_height)->default_value(800),
                "display window height")
            ("window_width,W",
                po::value<int>(&window_width)->default_value(800),
                "display window width");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help") || image_path == "" && regex_filter == "" ||
           mesh_filename == "")
        {
            std::cout << "Mesh file 2d viewing program" << std::endl;
            std::cout << "Version 1.00 2014 Jens Petersen" << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        return 1;
    }
    fs::path mesh_path(mesh_filename);
    if(mesh_path.extension() == ".off")
    {
        if(!off_load(mesh_filename.c_str(), vertex_count, vertices,
                     triangle_count, triangles)) return 1;
    }
    else if(mesh_path.extension() == ".mesh")
    {
        if(!mesh_load(mesh_filename.c_str(), vertex_count, vertices,
                      triangle_count, triangles)) return 1;
    }
    else
    {
        std::cerr << "ERROR: Unknown extension!" << std::endl;
        return 1;
    }
    std::cout << "Vertices: " << vertex_count
        << " triangles: " << triangle_count << std::endl;
    if(!cimg_load_series(image, image_path.c_str(), regex_filter.c_str()))
    {
        std::cerr << "ERROR: could not load file(s)!" << std::endl;
        return 1;
    }
    std::cout << "Image range: " << image.min() << " "
        << image.max() << std::endl;
    image -= image.min();
    image /= image.max();
    image_slice = image.depth()/2;
    int slice_size = std::max(image.width(), image.height());
    for(int i = 1; i < 4096; i*=2)
    {
        if(i >= slice_size)
        {
            slice_size = i;
            break;
        }
    }
    lines.assign(image.depth(), std::vector<float>());
    std::cout << "Image res: " << image.width() << "x" << image.height()
        << "x" << image.depth() << std::endl;
    std::cout << "Texture size: " << slice_size << "x" << slice_size
        << std::endl;
    slice.assign(3, slice_size, slice_size);
    for(int i = 0; i < vertex_count; i++)
    {
        vertices[i*3] = vertices[i*3]/(image.width()-1)*2-1;
        vertices[i*3+1] = -(vertices[i*3+1]/(image.height()-1)*2-1);
        vertices[i*3+2] = vertices[i*3+2]/(image.depth()-1)*2-1;
    }
    init_gl(ac, av);
    main_loop();
#ifdef WINDOWS_CONSOLE
    ctt.close();
#endif
    return 0;
}

